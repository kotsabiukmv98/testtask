using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TestTask.Core;
using Xunit;

namespace TestTask.Tests
{
    public class ProblemSolutionTest
    {
        [Fact]
        public async Task FindMaxSumAsync_ValidTextFile_SuccessfullyCalculated()
        {
            // Arrange 
            var problemSolution = new ProblemSolution();
            var filePath = "Test.txt";
            var expectedRowsWithMaxSum = new List<int> { 1 };
            var expectedBadRows = new List<int> { 3, 4, 5 };
            
            // Act
            var result = await problemSolution.FindMaxSumAsync(filePath);

            // Assert
            Assert.Equal(expectedRowsWithMaxSum, result.RowsWithMaxSum);
            Assert.Equal(expectedBadRows, result.BadRows);
        }

        [Fact]
        public async Task FindMaxSumAsync_InvalidTextFile_ExceptionThrown()
        {
            // Arrange 
            var problemSolution = new ProblemSolution();
            var filePath = @"C:\Documents\Newsletters\test.txt";
            var exceptionMessage = $"File \"{filePath}\" doesn't exist!";

            // Assert
            var exception = await Assert.ThrowsAsync<FileNotFoundException>(async () =>
                await problemSolution.FindMaxSumAsync(filePath));

            Assert.Equal(exceptionMessage, exception.Message);
        }
    }
}
