﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TestTask.Core;

namespace TestTask.ConsoleApp
{
    class Program
    {
        private static async Task Main()
        {
            Console.WriteLine("Program started...");

            IProblemSolution problemSolution = new ProblemSolution();

            GetFilePath(out var filePath);
            try
            {
                var result = await problemSolution.FindMaxSumAsync(filePath);
                PrintResult(result);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }
	    Console.ReadKey();
        }
        private static void PrintResult(MaxSumResult result)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            if (result.RowsWithMaxSum.Count == 0)
            {
                Console.WriteLine("There are no correct rows.");
            }
            if (result.RowsWithMaxSum.Count == 1)
            {
                Console.WriteLine($"Row with max sum:");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(result.RowsWithMaxSum[0]);
            }
            else
            {
                Console.WriteLine($"Rows with max sum:");
                PrintList(result.RowsWithMaxSum);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            if (result.BadRows.Count == 0)
            {
                Console.WriteLine("There are no bad rows.");
            }
            else if (result.BadRows.Count == 1)
            {
                Console.WriteLine($"Bad row:");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(result.BadRows[0]);
            }
            else
            {
                Console.WriteLine($"Bad rows:");
                PrintList(result.BadRows);
            }
        }
        private static void PrintList(List<int> list)
        {
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
        }
        private static void GetFilePath(out string filePath)
        {
            filePath = "";
            var args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                filePath = args[1];
            }
            else
            {
                Console.WriteLine("Enter file path, please:");
                filePath = Console.ReadLine();
            }
            while (!File.Exists(filePath))
            {
                Console.WriteLine("File does not exist, enter valid file path, please:");
                filePath = Console.ReadLine();
            }
        }
    }
}
