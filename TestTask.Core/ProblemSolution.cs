﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Core
{
    public class ProblemSolution : IProblemSolution
    {
        public async Task<MaxSumResult> FindMaxSumAsync(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException($"File \"{path}\" doesn't exist!");

            var maxSumResult = new MaxSumResult();

            using (var file = File.OpenText(path))
            {
                string row;
                var rowNumber = 1;
                var maxSum = decimal.MinValue;
                while ((row = await file.ReadLineAsync()) != null)
                {
                    var sumOfNumbers = ProcessRow(row);

                    if (sumOfNumbers == null)
                    {
                        maxSumResult.BadRows.Add(rowNumber++);
                        continue;
                    }

                    if (sumOfNumbers > maxSum)
                    {
                        maxSum = sumOfNumbers.Value;
                        maxSumResult.RowsWithMaxSum.Clear();
                        maxSumResult.RowsWithMaxSum.Add(rowNumber);
                    }
                    else if (sumOfNumbers == maxSum)
                    {
                        maxSumResult.RowsWithMaxSum.Add(rowNumber);
                    }
                    rowNumber++;
                }
            }

            return maxSumResult;
        }

        private decimal? ProcessRow(string row)
        {
            var numbers = row.Split(',');
            var isLineBad = numbers.Any(num => decimal.TryParse(num, out _) == false);

            if (isLineBad)
                return null;

            return numbers.Sum(num => decimal.Parse(num));
        }
    }
}
