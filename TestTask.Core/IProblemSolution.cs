﻿using System.Threading.Tasks;

namespace TestTask.Core
{
    public interface IProblemSolution
    {
        Task<MaxSumResult> FindMaxSumAsync(string path);
    }
}
