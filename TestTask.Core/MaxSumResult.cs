﻿using System.Collections.Generic;

namespace TestTask.Core
{
    public class MaxSumResult
    {
        public List<int> RowsWithMaxSum { get; set; } = new List<int>();
        public List<int> BadRows { get; set; } = new List<int>();
    }
}
